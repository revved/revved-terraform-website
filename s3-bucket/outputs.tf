output "name" {
  value = "${aws_s3_bucket.bucket.bucket}"
}

output "domain_name" {
  value = "${aws_s3_bucket.bucket.bucket_domain_name}"
}

output "regional_domain_name" {
  value = "${aws_s3_bucket.bucket.bucket_regional_domain_name}"
}
