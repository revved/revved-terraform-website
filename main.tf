# Data sources

data "http" "myip" {
  url = "http://ipv4.icanhazip.com"
}

# 1. EB

# SSL cert

module "ssl" {
  source = "./ssl"

  domain_name = "${var.website_domain}"
}

# Iam

module "iam" {
  source = "./iam"

  website_name = "${var.website_name}"
}

# EB App

module "eb-app" {
  source                     = "./eb-app"
  app_name                   = "${var.website_name}"
  app_description            = "${var.eb_app_description}"
  beanstalk_service_role_arn = "${module.iam.beanstalk_service_role_arn}"
}

# EB Env

module "eb-env" {
  source               = "./eb-env"
  app_name             = "${var.website_name}"
  app_env              = "${var.eb_app_env}"
  app_env_description  = "${var.eb_app_env_description}"
  cname_prefix         = "${var.website_name}"
  solution_stack_name  = "${var.eb_solution_stack_name}"
  security_group_name  = "${aws_security_group.eb_security_group.name}"
  iam_instance_profile = "${module.iam.instance_profile_id}"
  ssl_certificate_id   = "${module.ssl.arn}"
  service_role_name    = "${module.iam.beanstalk_service_role_name}"
  env_settings         = "${var.eb_env_settings}"
  website_domain       = "${var.website_domain}"
  website_subdomains   = "${var.website_subdomains}"
}

# EB Sec group

resource "aws_security_group" "eb_security_group" {
  name        = "${var.website_name}-sec-grp"
  description = "Controls access to and from ${var.website_name}"

  # Allow only secure traffic in and out
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Allow outgoing traffic to our DB
# Since both sec groups depend on each other, use `aws_security_group_rule` to prevent a cycle error
resource "aws_security_group_rule" "allow_egress_to_db" {
  type                     = "egress"
  from_port                = "${var.db_port}"
  to_port                  = "${var.db_port}"
  protocol                 = "tcp"
  security_group_id        = "${aws_security_group.eb_security_group.id}" // EB
  source_security_group_id = "${aws_security_group.db_security_group.id}" // DB
}

# 2. DB

module "database" {
  source                  = "./database"
  identifier              = "${var.db_identifier}"
  allocated_storage       = "${var.db_allocated_storage}"
  storage_type            = "${var.db_storage_type}"
  engine                  = "${var.db_engine}"
  engine_version          = "${var.db_engine_version}"
  port                    = "${var.db_port}"
  name                    = "${var.db_name}"
  username                = "${var.db_username}"
  password                = "${var.db_password}"
  parameter_group_name    = "${var.db_parameter_group_name}"
  backup_retention_period = "${var.db_backup_retention_period}"
  backup_window           = "${var.db_backup_window}"
  maintenance_window      = "${var.db_maintenance_window}"
  storage_encrypted       = "${var.db_storage_encrypted}"
  instance_class          = "${var.db_instance_class}"
  publicly_accessible     = "${var.db_publicly_accessible}"
  skip_final_snapshot     = "${var.db_skip_final_snapshot}"
  vpc_security_group_ids  = ["${aws_security_group.db_security_group.id}"]
}

# DB Sec group

resource "aws_security_group" "db_security_group" {
  name = "${var.website_name}-db-sec-grp"

  // Only allow connections from and to our site
  ingress {
    from_port       = "${var.db_port}"
    to_port         = "${var.db_port}"
    protocol        = "tcp"
    security_groups = ["${aws_security_group.eb_security_group.id}"]
  }

  egress {
    from_port       = "${var.db_port}"
    to_port         = "${var.db_port}"
    protocol        = "tcp"
    security_groups = ["${aws_security_group.eb_security_group.id}"]
  }

  // And our IP address. At least during db setup.
  // Remove after orovisioning if needed
  ingress {
    from_port   = "${var.db_port}"
    to_port     = "${var.db_port}"
    protocol    = "tcp"
    cidr_blocks = ["${chomp(data.http.myip.body)}/32"]
  }

  egress {
    from_port   = "${var.db_port}"
    to_port     = "${var.db_port}"
    protocol    = "tcp"
    cidr_blocks = ["${chomp(data.http.myip.body)}/32"]
  }
}

# 3. S3

module "s3-bucket" {
  source             = "./s3-bucket"
  name               = "${var.website_name}-storage"
  acl                = "${var.s3_bucket_acl}"
  aws_region         = "${var.aws_region}"
  principal_role_arn = "${module.iam.instance_profile_role_arn}"
}

# 4. SES

module "ses" {
  source         = "./ses"
  website_name   = "${var.website_name}"
  website_domain = "${var.website_domain}"
  aws_region     = "${var.aws_region}"
}
