output "aws_region" {
  value = "${var.aws_region}"
}

output "aws_website_profile" {
  value = "${var.aws_website_profile}"
}

output "aws_dns_profile" {
  value = "${var.aws_dns_profile}"
}

output "db_endpoint" {
  value = "${module.database.endpoint}"
}

output "db_address" {
  value = "${module.database.address}"
}

output "db_instance_id" {
  value = "${module.database.instance_id}"
}

output "db_username" {
  value = "${module.database.username}"
}

output "db_name" {
  value = "${module.database.name}"
}

output "eb_app_name" {
  value = "${module.eb-app.name}"
}

output "eb_app_env" {
  value = "${var.eb_app_env}"
}

output "s3_bucket_name" {
  value = "${module.s3-bucket.name}"
}

output "s3_domain_name" {
  value = "${module.s3-bucket.domain_name}"
}

output "s3_regional_domain_name" {
  value = "${module.s3-bucket.regional_domain_name}"
}

# `smsn` stands for `secret manager secret name`
output "ses_smtp_access_key_smsn" {
  value = "${module.ses.smtp_access_key_smsn}"
}

output "ses_smtp_password_smsn" {
  value = "${module.ses.smtp_password_smsn}"
}

output "ses_smtp_host" {
  value = "${module.ses.smtp_host}"
}
