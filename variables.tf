variable "aws_region" {}
variable "aws_website_profile" {}
variable "aws_dns_profile" {}
variable "website_name" {}
variable "website_domain" {}

variable "website_subdomains" {
  type = "list"
}

# EB

variable "eb_app_env" {}
variable "eb_app_description" {}
variable "eb_app_env_description" {}
variable "eb_cname_prefix" {}
variable "eb_solution_stack_name" {}

variable "eb_env_settings" {
  type = "list"
}

# DB

variable "db_identifier" {}
variable "db_allocated_storage" {}
variable "db_storage_type" {}
variable "db_engine" {}
variable "db_engine_version" {}
variable "db_port" {}
variable "db_name" {}
variable "db_username" {}
variable "db_password" {}
variable "db_parameter_group_name" {}
variable "db_backup_retention_period" {}
variable "db_backup_window" {}
variable "db_maintenance_window" {}
variable "db_storage_encrypted" {}
variable "db_instance_class" {}
variable "db_publicly_accessible" {}
variable "db_skip_final_snapshot" {}

# S3

variable "s3_bucket_acl" {}
