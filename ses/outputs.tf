output "smtp_access_key_smsn" {
  value = "${aws_secretsmanager_secret.ses_smtp_access_key.name}"
}

output "smtp_password_smsn" {
  value = "${aws_secretsmanager_secret.ses_smtp_password.name}"
}

output "smtp_host" {
  value = "ssl://email-smtp.${var.aws_region}.amazonaws.com"
}
