# Important:
# To send emails we need to either:
# - move out of the sandbox: https://docs.aws.amazon.com/ses/latest/DeveloperGuide/request-production-access.html?icmpid=docs_ses_console
# - verify each email address we want to send emails to: https://docs.aws.amazon.com/ses/latest/DeveloperGuide/verify-email-addresses.html
# - send emails under a domain we own? Based on some testing i was able to send emails to addresses ending in our domain

data "aws_route53_zone" "zone" {
  // Use DNS provider
  provider     = "aws.dns"
  name         = "${var.website_domain}."
  private_zone = false
}

# SES domain verification

resource "aws_ses_domain_identity" "default" {
  domain = "${var.website_domain}"
}

resource "aws_route53_record" "ses_verification_record" {
  // Use DNS provider
  provider = "aws.dns"
  zone_id  = "${data.aws_route53_zone.zone.id}"
  name     = "_amazonses.${var.website_domain}"
  type     = "TXT"
  ttl      = "600"
  records  = ["${aws_ses_domain_identity.default.verification_token}"]
}

resource "aws_ses_domain_identity_verification" "default" {
  domain = "${var.website_domain}"

  depends_on = ["aws_route53_record.ses_verification_record"]
}

# SES from domain verification

resource "aws_ses_domain_mail_from" "default" {
  domain           = "${aws_ses_domain_identity.default.domain}"
  mail_from_domain = "mail.${aws_ses_domain_identity.default.domain}"
}

# Example Route53 MX record
resource "aws_route53_record" "ses_domain_mail_from_mx" {
  // Use DNS provider
  provider = "aws.dns"
  zone_id  = "${data.aws_route53_zone.zone.id}"
  name     = "${aws_ses_domain_mail_from.default.mail_from_domain}"
  type     = "MX"
  ttl      = "600"
  records  = ["10 feedback-smtp.${var.aws_region}.amazonses.com"]
}

# Example Route53 TXT record for SPF
resource "aws_route53_record" "ses_domain_mail_from_txt" {
  // Use DNS provider
  provider = "aws.dns"
  zone_id  = "${data.aws_route53_zone.zone.id}"
  name     = "${aws_ses_domain_mail_from.default.mail_from_domain}"
  type     = "TXT"
  ttl      = "600"
  records  = ["v=spf1 include:amazonses.com -all"]
}

# SES dkim

resource "aws_ses_domain_dkim" "default" {
  domain = "${aws_ses_domain_identity.default.domain}"
}

resource "aws_route53_record" "ses_dkim_records" {
  count    = 3
  provider = "aws.dns"
  zone_id  = "${data.aws_route53_zone.zone.id}"
  name     = "${element(aws_ses_domain_dkim.default.dkim_tokens, count.index)}._domainkey.${var.website_domain}"
  type     = "CNAME"
  ttl      = "600"
  records  = ["${element(aws_ses_domain_dkim.default.dkim_tokens, count.index)}.dkim.amazonses.com"]
}

# SES smpt user

resource "aws_iam_access_key" "ses_iam_user_key" {
  user = "${aws_iam_user.ses_iam_user.name}"
}

resource "aws_iam_user" "ses_iam_user" {
  name = "${var.website_name}-ses-user"
}

resource "aws_iam_user_policy" "ses_iam_user_policy" {
  name = "${var.website_name}-ses-user-policy"
  user = "${aws_iam_user.ses_iam_user.name}"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": "ses:SendRawEmail",
            "Resource": "*"
        }
    ]
}
EOF
}

# Secrets

resource "aws_secretsmanager_secret" "ses_smtp_access_key" {
  name = "${var.website_name}_ses_smtp_user"
}

resource "aws_secretsmanager_secret_version" "ses_smtp_access_key" {
  secret_id     = "${aws_secretsmanager_secret.ses_smtp_access_key.id}"
  secret_string = "${aws_iam_access_key.ses_iam_user_key.id}"
}

resource "aws_secretsmanager_secret" "ses_smtp_password" {
  name = "${var.website_name}_ses_smtp_pass"
}

resource "aws_secretsmanager_secret_version" "ses_smtp_password" {
  secret_id     = "${aws_secretsmanager_secret.ses_smtp_password.id}"
  secret_string = "${aws_iam_access_key.ses_iam_user_key.ses_smtp_password}"
}
