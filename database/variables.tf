variable "identifier" {}
variable "allocated_storage" {}
variable "storage_type" {}
variable "engine" {}
variable "engine_version" {}
variable "port" {}
variable "name" {}
variable "username" {}
variable "password" {}
variable "parameter_group_name" {}
variable "backup_retention_period" {}
variable "backup_window" {}
variable "maintenance_window" {}
variable "storage_encrypted" {}
variable "instance_class" {}
variable "publicly_accessible" {}
variable "skip_final_snapshot" {}

variable "vpc_security_group_ids" {
  type = "list"
}
