output "arn" {
  value = "${aws_db_instance.default.arn}"
}

output "endpoint" {
  value = "${aws_db_instance.default.endpoint}"
}

output "address" {
  value = "${aws_db_instance.default.address}"
}

output "port" {
  value = "${aws_db_instance.default.port}"
}

output "instance_id" {
  value = "${aws_db_instance.default.id}"
}

output "username" {
  value = "${aws_db_instance.default.username}"
}

output "name" {
  value = "${aws_db_instance.default.name}"
}
