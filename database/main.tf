resource "aws_db_instance" "default" {
  identifier              = "${var.identifier}"
  allocated_storage       = "${var.allocated_storage}"
  storage_type            = "${var.storage_type}"
  engine                  = "${var.engine}"
  engine_version          = "${var.engine_version}"
  port                    = "${var.port}"
  name                    = "${var.name}"
  username                = "${var.username}"
  password                = "${var.password}"
  parameter_group_name    = "${var.parameter_group_name}"
  backup_retention_period = "${var.backup_retention_period}"
  backup_window           = "${var.backup_window}"
  maintenance_window      = "${var.maintenance_window}"
  storage_encrypted       = "${var.storage_encrypted}"
  instance_class          = "${var.instance_class}"
  publicly_accessible     = "${var.publicly_accessible}"
  skip_final_snapshot     = "${var.skip_final_snapshot}"
  vpc_security_group_ids  = ["${var.vpc_security_group_ids}"]

  lifecycle {
    ignore_changes = ["password"]
  }
}
