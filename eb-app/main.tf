resource "aws_elastic_beanstalk_application" "website_beanstalk_application" {
  name        = "${var.app_name}"
  description = "${var.app_description}"

  appversion_lifecycle {
    service_role          = "${var.beanstalk_service_role_arn}"
    max_count             = 128
    delete_source_from_s3 = true
  }
}
