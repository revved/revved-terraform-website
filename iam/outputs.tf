# Instance profile
output "instance_profile_id" {
  value = "${aws_iam_instance_profile.instance_profile.id}"
}

output "instance_profile_arn" {
  value = "${aws_iam_instance_profile.instance_profile.arn}"
}

output "instance_profile_name" {
  value = "${aws_iam_instance_profile.instance_profile.name}"
}

output "instance_profile_role_arn" {
  value = "${aws_iam_role.instance_profile_role.arn}"
}

# Service role

output "beanstalk_service_role_name" {
  value = "${aws_iam_role.beanstalk_service_role.name}"
}

output "beanstalk_service_role_arn" {
  value = "${aws_iam_role.beanstalk_service_role.arn}"
}
