variable "eb_env_cname" {}
variable "eb_zone_id" {}
variable "domain_name" {}

variable "subdomains" {
  type = "list"
}
