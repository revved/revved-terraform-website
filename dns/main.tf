# Data sources

data "aws_route53_zone" "zone" {
  // Use DNS provider
  provider     = "aws.dns"
  name         = "${var.domain_name}."
  private_zone = false
}

resource "aws_route53_record" "default" {
  // Use DNS provider
  provider = "aws.dns"
  count    = "${length(var.subdomains)}"
  zone_id  = "${data.aws_route53_zone.zone.id}"
  name     = "${element(var.subdomains, count.index)}"
  type     = "A"

  alias {
    name                   = "${var.eb_env_cname}"
    zone_id                = "${var.eb_zone_id}"
    evaluate_target_health = false
  }
}
