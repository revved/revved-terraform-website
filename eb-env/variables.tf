variable "cname_prefix" {}
variable "app_name" {}
variable "app_env" {}
variable "app_env_description" {}
variable "solution_stack_name" {}
variable "security_group_name" {}
variable "iam_instance_profile" {}
variable "ssl_certificate_id" {}
variable "service_role_name" {}
variable "website_domain" {}

variable "website_subdomains" {
  type = "list"
}

variable "env_settings" {
  type = "list"
}
