data "aws_elastic_beanstalk_hosted_zone" "current" {
  # provider = "aws.dns"
}

resource "aws_elastic_beanstalk_environment" "default" {
  tier                = "WebServer"
  name                = "${var.app_env}"
  description         = "${var.app_env_description}"
  application         = "${var.app_name}"
  solution_stack_name = "${var.solution_stack_name}"
  cname_prefix        = "${var.cname_prefix}"

  setting = ["${var.env_settings}"]

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "SecurityGroups"
    value     = "${var.security_group_name}"
  }

  setting {
    namespace = "aws:autoscaling:launchconfiguration"
    name      = "IamInstanceProfile"
    value     = "${var.iam_instance_profile}"
  }

  # setting {
  #   namespace = "aws:elb:listener:80"
  #   name      = "SSLCertificateId"
  #   value     = "${var.ssl_certificate_id}"
  # }

  setting {
    namespace = "aws:elb:listener:443"
    name      = "SSLCertificateId"
    value     = "${var.ssl_certificate_id}"
  }
  setting {
    namespace = "aws:elasticbeanstalk:environment"
    name      = "ServiceRole"
    value     = "${var.service_role_name}"
  }
}

# Custom domains for our site
module "dns" {
  source = "../dns"

  eb_env_cname = "${aws_elastic_beanstalk_environment.default.cname}"
  eb_zone_id   = "${data.aws_elastic_beanstalk_hosted_zone.current.id}"
  domain_name  = "${var.website_domain}"
  subdomains   = "${var.website_subdomains}"
}
